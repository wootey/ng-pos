var app = angular.module("myApp", ['ui.router']);
app.config(function($stateProvider, $urlRouterProvider) { 
    $urlRouterProvider.otherwise(
    function($injector, $location) {
      $location.path('/homepage');
    });

    $stateProvider       
        .state('home', {
            url: '/home',
            abstract: true,
            templateUrl: 'index.html',
            controller: 'kmeans'
        }).state('homepage', {
            url: '/homepage',
            views: {
                'homepage': {
                    templateUrl: 'templates/home.html',
                    controller: 'kmeans'
                }
            }
        }).state('ReportAll', {
            url: '/ReportAll',
            views: {
                'ReportAll': {
                    templateUrl: 'templates/menulist/ReportAll.html',
                    controller: 'kmeans'
                },
                'BlackToHome': {
                    templateUrl: 'templates/menubar/blackToHome.html',
                    controller: 'kmeans'
                }
            }
        }).state('AssetAll', {
            url: '/AssetAll',
            views: {
                'AssetAll': {
                    templateUrl: 'templates/menulist/AssetAll.html',
                    controller: 'kmeans'
                },
                'BlackToHome': {
                    templateUrl: 'templates/menubar/blackToHome.html',
                    controller: 'kmeans'
                }
            }
        }).state('AddAsset', {
            url: '/AddAsset',
            views: {
                'AddAsset': {
                    templateUrl: 'templates/AddAsset.html',
                    controller: 'kmeans'
                },
                'MenuAddAsset': {
                    url:'/MenuAddAsset',
                    templateUrl: 'templates/menubar/MenuAddAsset.html',
                    controller: 'kmeans'
                }
            }
        }).state('Order', {
            url: '/Order',
            views: {
                'Order': {
                    templateUrl: 'templates/Order.html',
                    controller: 'kmeans'
                },
                'MenuOrder': {
                    url:'/MenuOrder',
                    templateUrl: 'templates/menubar/MenuOrder.html',
                    controller: 'kmeans'
                }
            }
        }).state('addemployee', {
            url: '/addemployee',
            views: {
                'addemployee': {
                    url:'/addemployee',
                    templateUrl: 'templates/addemployee.html',
                    controller: 'kmeans'
                }
            }
         }).state('newhtml', {
            url: '/newhtml',
            views: {
                'newhtml': {
                    url:'/newhtml',
                    templateUrl: 'templates/newhtml.html',
                    controller: 'kmeans'
                }
            }
        }).state('AddItem', {
            url: '/AddItem',
            views: {
                'AddItem': {
                    url:'/AddItem',
                    templateUrl: 'templates/AddItem.html',
                    controller: 'kmeans'
                }
            }
        });

}).directive('loading', function () {
      return {
        restrict: 'E',
        replace:true,
        template: '<div ng-show="loading" class="loading"><img src="img/spinner.gif"/>LOADING...</div>',
        link: function (scope, element, attr) {
            scope.$watch('loading', function (val) {
                if (val)
                    $(element).show();
                else
                    $(element).hide();
            });
        }
      }
}).directive('fileModel', ['$parse', function ($parse) {
    return {
       restrict: 'A',
       link: function(scope, element, attrs) {
          var model = $parse(attrs.fileModel);
          var modelSetter = model.assign;

          element.bind('change', function(){
             scope.$apply(function(){
                modelSetter(scope, element[0].files[0]);
             });
          });
       }
    };
}]);


